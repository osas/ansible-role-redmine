# Ansible role to manage Redmine installation

## Introduction

[Phusion Passenger](https://www.phusionpassenger.com/) is a web application server.

This role configures an Apache Httpd vhost with Passenger, and installs Redmine.
The necessary RDBMS needs to be ready beforehand.
It supports installation on Debian OS family only at the moment because Redmine is not packaged in the RedHat family.

This role also expects to work hand-in hand with the OSAS httpd role (ansible-role-ah-httpd).
You can pass httpd extra parameters to this role and the vhost creation will use them.

Here is a complete example:
```
    - name: Install Redmine
      include_role:
        name: redmine
      vars:
        instance: myprojects
        use_letsencrypt: True
        force_tls: True
```

## Variables

- **instance**: Redmine instance name associated to this vhost
- **database**: RDBMS type to use (pgsql/mysql, defaults to pgsql)
- **database_cipher_key**: key to encrypt sensive information in the database (recommanded)

